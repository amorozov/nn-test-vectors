#!/usr/bin/python
import random
from mpmath import mp, nstr

mp.prec = 300 # 111 for f128
print_prec = 19 # 34 for f128, 16 for f64

def group(xs, n):
    for i in range(0, len(xs), n):
        yield xs[i:i+n]

def print_arr(name, xs):
    print 'const %s: [f64; %d] =' % (name.upper(), len(xs))
    lines = list(group(xs, 3))
    for i, gs in enumerate(lines):
        if i == 0:
            fmt = "    [%s,"
        elif i == len(lines) - 1:
            fmt = "     %s];"
        else:
            fmt = "     %s,"
        # line = ', '.join(str(x) for x in gs)
        line = ', '.join(nstr(x, print_prec) for x in gs)
        print fmt % str(line)
    print

def rand_vec(n, x0, x1):
    return [mp.rand() * (x1 - x0) + x0 for _ in xrange(n)]

random.seed("rGeimi4pCh1UoD3W") # yes, it's random enough


print '---------------------------------------------------------- softmax'
softmax_n = 12
softmax_in = rand_vec(softmax_n, -1.5, 1.5)
softmax_out_grad = rand_vec(softmax_n, -5, 5)

def softmax(xs):
    es = map(mp.exp, xs)
    return [e / sum(es) for e in es]

def softmax_grad(xs, dxs):
    s = sum(x * dx for x, dx in zip(xs, dxs))
    return [(x * (dx - s)) for x, dx in zip(xs, dxs)]

def log_softmax(xs):
    return [mp.log(y) for y in softmax(xs)]

def log_softmax_grad(xs, dxs):
    s = sum(dxs)
    return [dx - mp.exp(x) * s for x, dx in zip(xs, dxs)]

softmax_out = softmax(softmax_in)
softmax_in_grad = softmax_grad(softmax_out, softmax_out_grad)
log_softmax_out = log_softmax(softmax_in)
log_softmax_in_grad = log_softmax_grad(log_softmax_out, softmax_out_grad)

print_arr('in', softmax_in)
print_arr('out_grad', softmax_out_grad)
print_arr('softmax_out', softmax_out)
print_arr('softmax_in_grad', softmax_in_grad)
print_arr('log_softmax_out', log_softmax_out)
print_arr('log_softmax_in_grad', log_softmax_in_grad)


print '---------------------------------------------------------- activation'
def relu(xs):
    return [x * (1 if x > 0 else 0) for x in xs]

def relu_grad(xs, dxs):
    return [(1 if x > 0 else 0) * dx for (x, dx) in zip(xs, dxs)]

def sigmoid(xs):
    return [1 / (1 + mp.exp(-x)) for x in xs]

def sigmoid_grad(xs, dxs):
    return [x * (1 - x) * dx for x, dx in zip(xs, dxs)]

def tanh(xs):
    return [mp.tanh(x) for x in xs]

def tanh_grad(xs, dxs):
    return [(1 - x * x) * dx for x, dx in zip(xs, dxs)]


act_n = 12
act_in = rand_vec(act_n, -2, 2)
act_out_grad = rand_vec(act_n, -3, 3)

relu_out = relu(act_in)
relu_in_grad = relu_grad(relu_out, act_out_grad)

sigmoid_out = sigmoid(act_in)
sigmoid_in_grad = sigmoid_grad(sigmoid_out, act_out_grad)

tanh_out = tanh(act_in)
tanh_in_grad = tanh_grad(tanh_out, act_out_grad)

print_arr('in', act_in)
print_arr('out_grad', act_out_grad)
print_arr('relu_out', relu_out)
print_arr('relu_in_grad', relu_in_grad)
print_arr('sigmoid_out', sigmoid_out)
print_arr('sigmoid_in_grad', sigmoid_in_grad)
print_arr('tanh_out', tanh_out)
print_arr('tanh_in_grad', tanh_in_grad)

